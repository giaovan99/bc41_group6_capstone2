// import { renderProductList, filterBrand} from "./controllers/controllers.js";

let productList=[];
let samsungList=[];
let iphoneList=[];
let cart=[];

let BASE_URL='https://63dbd8cec45e08a0434f8f05.mockapi.io/product'
// chay lan dau khi load trang
let fetchProductList = () => {
    setTimeout(() => {
        axios({
            url:BASE_URL,
            method:"GET"
        })
        .then((res) => {
            productList.push(...res.data);
            filterBrand(productList,samsungList,iphoneList);
            renderProductList(productList);
        })
        .catch((err) =>{
            console.log(err);
        })
    }, 0);
};
// chay lan dau khi load trang
fetchProductList();

// onChange
let onChange=()=>{
    let value=document.getElementById('typeProduct').value;
    if(value==0){
        renderProductList(productList);
    } else if(value==1){
        renderProductList(samsungList);
    } else{
        renderProductList(iphoneList);
    };
};
// window.onChange = onChange;
var cartItem = {
    product:{},
    quantity:1,
};

function addToCart(index){
    var a= productList[index].id;
    var b= productList[index].name;
    var c= productList[index].price;
    count=0;
    cartItem={
                product: {
                    id:a,
                    price:c,
                    name:b,
                },
                quantity:1,
            };
    for(var i=0; i<cart.length; i++){
        if(cart[i].product.id==cartItem.product.id){
            cart[i].quantity+=1;
            count=1;
        };
    };
    if(count==0){
        cart.push(cartItem);
    };
    console.log(cart);
    renderTotalQuantity();
};

function removeFromCart(index){
    var a= productList[index].id;
}

// window.addToCart=addToCart;
let renderProductList = (productArr) => {
    let contentHTML='';
    productArr.forEach((item,index) =>{
        let divTag=`
                <div class="col-3">
                    <div>
                        <img src="${item.img}" style="width:100%">
                        <div >${item.name}</div>
                        <div >${item.price}</div>
                        <button id="${item.id}" class="btn btn-success" onclick="addToCart(${index})">Add</button>                        
                    </div>
                </div>               
        `
        contentHTML+=divTag;
        document.getElementById('listProducts').innerHTML=contentHTML;
    })
};

// phan loai thuong hieu
let filterBrand = (productArr,samsungList,iphoneList) => {
    productArr.filter(product =>{ 
        if (product.type=="Samsung"){
            samsungList.push(product);
        } else{
            iphoneList.push(product);
        }
    })
};

// render total quantity
function renderTotalQuantity(){
    let totalQuantity=0;
    cart.forEach((item) =>{
        totalQuantity+=item.quantity;
    });
    document.getElementById('number').innerHTML=totalQuantity;
}


